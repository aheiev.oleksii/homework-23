const obj1 = {
  x: 10,
  y: 20,
};

const obj2 = {
  z: 30,
};

const objA = {
  x: 10,
};

const objB = {
  x: 20,
  y: 30,
};

function assignObjects(objA, objB) {
  let newObj = {};

  for (let key in objA) {
    newObj[key] = objA[key];
  }
  for (let key in objB) {
    newObj[key] = objB[key];
  }

  return newObj;
}

console.log(assignObjects(obj1, obj2));
console.log(assignObjects(objA, objB));
